# rd

### Trial2 index placeholder
replace the Trial2 to your site's pages

### Generate cert for testing
openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
    -subj "/C=HK/ST=HK/L=HK/O=RD/CN=localhost" \
    -keyout localhost.key  -out localhost.cert

### Docker build
```
docker build -t rdnginx -f Dockerfile .
```

### Docker run
```
docker run --name mynginx -p 80:80 -p 443:443 -d rdnginx
```

http://localhost/  
https://localhost/
