FROM nginx
RUN apt-get update && apt-get install -y curl vim
RUN mkdir -p /srv/rd-web /etc/nginx/ssl/

RUN rm /etc/nginx/conf.d/default.conf
COPY robot-data.com.conf /etc/nginx/conf.d/
COPY Trial2 /srv/rd-web

COPY localhost.cert  /etc/nginx/ssl/localhost.cert
COPY localhost.key  /etc/nginx/ssl/localhost.key

RUN service nginx restart
